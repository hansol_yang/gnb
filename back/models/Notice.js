var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var Notice = new Schema({
    title: String,
    body: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    editedAt: {
        type: Date,
        default: Date.now
    },
    isEdited: {
        type: Boolean,
        default: false
    },
    _creator: {
        _id: Schema.Types.ObjectId,
        username: String,
        isAdmin: Boolean
    }
});

var notices = mongoose.model('notice', Notice);


module.exports = notices;