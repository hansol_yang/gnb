var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var DevNote = new Schema({
    title: String,
    body: String,
    createAt: {
        type: Date,
        default: Date.now
    },
    editedAt: {
        type: Date,
        default: Date.now
    },
    isEdited: {
        type: Boolean,
        default: false
    },
    _creator: {
        _id: Schema.Types.ObjectId,
        username: String,
        isAdmin: Boolean
    }
});

var devNotes = mongoose.model('devNote', DevNote);


module.exports = devNotes;