var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var Post = new Schema({
    title: String,
    body: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    editedAt: {
        type: Date,
        default: Date.now
    },
    isEdited: {
        type: Boolean,
        default: false
    },
    _creator: {
        _id: Schema.Types.ObjectId,
        username: String,
        isAdmin: Boolean
    }
});

var posts = mongoose.model('post', Post);


module.exports = posts;