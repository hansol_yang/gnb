var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var User = new Schema({
    username: String,
    password: String,
    isAdmin: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    editedAt: {
        type: Date,
        default: Date.now
    },
    isEdited: {
        type: Boolean,
        default: false
    }
});

var users = mongoose.model('user', User);


module.exports = users;