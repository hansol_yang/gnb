var Notice = require('../../../models/Notice');

/*
 * POST: /notices
 * {
 *  title,
 *  body
 * }
 * */
exports.createNotice = function (req, res) {
    var title = req.body.title;
    var body = req.body.body;
    var decoded = req.decoded;

    var createNotice = new Promise(function (resolve, reject) {
        if (!decoded.isAdmin) {
            reject(new Error('PERMISSION DENIED'));
        } else {
            var notice = new Notice({
                title: title,
                body: body,
                _creator: {
                    _id: decoded._id,
                    username: decoded.username,
                    isAdmin: decoded.isAdmin
                }
            });

            notice.save(function (err) {
                if (err) reject(err);
                resolve(notice);
            })
        }
    });

    function respond(notice) {
        return res.json({
            msg: '공지 사항 작성에 성공했습니다',
            code: 300,
            result: notice
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    createNotice.then(respond)
        .catch(onError);
};


/*
 * GET: /notices
 * */
exports.readAllNotices = function (req, res) {
    var readAllNotices = new Promise(function (resolve, reject) {
        Notice.find({}, function (err, data) {
            if (err) reject(err);
            resolve(data);
        })
    });

    function respond(data) {
        return res.json({
            msg: '공지 사항 전체 조회에 성공했습니다',
            code: 300,
            result: data
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    readAllNotices.then(respond)
        .catch(onError);
};