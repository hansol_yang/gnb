var router = require('express').Router();

var controller = require('./notices.controller');
var auth = require('../auth');


router.post('/', auth);
router.post('/', controller.createNotice);

router.get('/', controller.readAllNotices);

module.exports = router;