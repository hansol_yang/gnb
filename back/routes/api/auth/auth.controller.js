/*
 * GET: /auth/getUserInfo
 * */
exports.getUserInfo = function (req, res) {
    var decoded = req.decoded;
    return res.json({
        msg: '회원 검증에 성공했습니다',
        code: 300,
        result: decoded
    })
};