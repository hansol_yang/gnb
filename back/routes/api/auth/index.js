var jwt = require('jsonwebtoken');


module.exports = function (req, res, next) {
    var token = req.headers['x-access-token'];
    var secret = req.app.get('jwt-secret');

    if (!token) {
        return res.json({
            msg: '로그인이 되지 않았습니다',
            code: 404
        })
    }

    var p = new Promise(function (resolve, reject) {
        jwt.verify(token, secret, function (err, decoded) {
            if (err) reject(err);
            resolve(decoded);
        })
    });

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    p.then(function (decoded) {
        req.decoded = decoded;
        next();
    }).catch(onError);
};