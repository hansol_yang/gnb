var Post = require('../../../models/Post');

/*
 * POST: /posts
 * {
 *   title,
 *   body
 * }
 * */
exports.createPost = function (req, res) {
    var title = req.body.title;
    var body = req.body.body;
    var decoded = req.decoded;



    var createPost = new Promise(function (resolve, reject) {
        var post = new Post({
            title: title,
            body: body,
            _creator: {
                _id: decoded._id,
                username: decoded.username,
                isAdmin: decoded.isAdmin
            }
        });

        post.save(function (err) {
            if (err) reject(err);
            resolve(post);
        })
    });

    function respond(post) {
        return res.json({
            msg: '일반 게시물 작성에 성공했습니다',
            code: 300,
            result: post
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    createPost.then(respond).catch(onError);
};


/*
 * GET: /posts
 * */
exports.readAllPosts = function (req, res) {
    var readAllPosts = new Promise(function (resolve, reject) {
        Post.find({}, function (err, data) {
            if (err) reject(err);
            resolve(data);
        });
    });

    function respond(data) {
        return res.json({
            msg: '일반 게시물 전체 조회에 성공했습니다',
            code: 300,
            posts: data
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    readAllPosts.then(respond)
        .catch(onError);
};


/*
 * GET: /posts/:id
 * */
exports.readPost = function (req, res) {
    var readPost = new Promise(function (resolve, reject) {
        Post.findOne({_id: req.params.id}, function (err, data) {
            if (err) reject(err);
            resolve(data);
        })
    });

    function respond(data) {
        return res.json({
            msg: '일반 게시물 조회에 성공했습니다',
            code: 300,
            post: data
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    readPost.then(respond)
        .catch(onError);
};