var router = require('express').Router();

var controller = require('./posts.controller');
var auth = require('../auth');


router.post('/', auth);
router.post('/', controller.createPost);

router.get('/', controller.readAllPosts);
router.get('/:id', controller.readPost);


module.exports = router;