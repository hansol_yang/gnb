var DevNote = require('../../../models/DevNote');


/*
 * POST: /devNotes
 * {
 *   title,
 *   body
 * }
 * */
exports.createDevNote = function (req, res) {
    var title = req.body.title;
    var body = req.body.body;
    var decoded = req.decoded;

    var createDevNote = new Promise(function (resolve, reject) {
        var devNote = new DevNote({
            title: title,
            body: body,
            _creator: {
                _id: decoded._id,
                username: decoded.username,
                isAdmin: decoded.isAdmin
            }
        });

        devNote.save(function (err) {
            if (err) reject(err);
            resolve(devNote);
        })
    });

    function respond(devNote) {
        return res.json({
            msg: '개발 일지 작성에 성공했습니다',
            code: 300,
            result: devNote
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    createDevNote.then(respond)
        .catch(onError);
};


/*
 * GET: /devNotes
 * */
exports.readAllDevNotes = function (req, res) {
    var readAllDevNotes = new Promise(function (resolve, reject) {
        DevNote.find({}, function (err, data) {
            if (err) reject(err);
            resolve(data);
        })
    });

    function respond(data) {
        return res.json({
            msg: '개발 일지 전체 조회에 성공했습니다',
            code: 300,
            devNotes: data
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    readAllDevNotes.then(respond)
        .catch(onError);
};


/*
* GET: /devNotes/:id
* */
exports.readDevNote = function(req, res) {
    var readDevNote = new Promise(function(resolve, reject) {
        DevNote.findOne({_id: req.params.id}, function(err, data) {
            if(err) reject(err);
            resolve(data);
        })
    });

    function respond(data) {
        return res.json({
            msg: '개발 일지 조회에 성공했습니다',
            code: 300,
            devNote: data
        })
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    readDevNote.then(respond)
        .catch(onError);
};