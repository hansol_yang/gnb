var router = require('express').Router();

var controller = require('./devNotes.controller');
var auth = require('../auth');


router.post('/', auth);
router.post('/', controller.createDevNote);

router.get('/', controller.readAllDevNotes);
router.get('/:id', controller.readDevNote);


module.exports = router;