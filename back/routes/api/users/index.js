var router = require('express').Router();

var controller = require('./users.controller');


router.post('/register', controller.register);
router.post('/login', controller.login);


module.exports = router;