var User = require('../../../models/User');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');

/*
 * POST /users/register
 * {
 *   username,
 *   password
 * }
 * */
exports.register = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;

    function findOneByUsername(name) {
        return User.findOne({username: name}).exec();
    }

    function createNewAccount(user) {
        if(user) {
            return res.json({
                msg: '이미 등록된 회원입니다',
                code: 401
            })
        } else {
            bcrypt.hash(password, 10, function(err, hash) {
                if(err) throw(err);

                var user = new User({
                    username: username,
                    password: hash
                });

                user.save(function(err) {
                    if(err) throw(err);

                    return res.json({
                        msg: '회원가입에 성공했습니다',
                        code: 940924,
                        userInfo: user
                    })
                })
            })
        }
    }

    function onError(err) {
        return res.json({
            msg: err.message
        })
    }

    findOneByUsername(username)
        .then(createNewAccount)
        .catch(onError);
};


/*
 * POST /users/login
 * {
 *  username,
 *  password
 * }
 * */
exports.login = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var secret = req.app.get('jwt-secret');

    function findOneByUsername(name) {
        return User.findOne({username: name}).exec();
    }

    function checkPassword(user) {
        if (!user) {
            return res.json({
                msg: '등록되지 않은 회원입니다',
                code: 404
            })
        } else {
            bcrypt.compare(password, user.password, function (err, isMatch) {
                if (err) throw(err);

                if (!isMatch) {
                    return res.json({
                        msg: '비밀 번호가 일치하지 않습니다',
                        code: 401
                    });
                } else {
                    jwt.sign(
                        {
                            _id: user._id,
                            username: user.username,
                            isAdmin: user.isAdmin
                        },
                        secret,
                        {
                            expiresIn: '6h',
                            issuer: 'gnb.com',
                            subject: 'userInfo'
                        }, function (err, token) {
                            if (err) throw(err);

                            return res.json({
                                msg: '로그인에 성공했습니다',
                                code: 930709,
                                token: token
                            })
                        }
                    )
                }
            })
        }
    }

    function onError(err) {
        return res.json({
            msg: err.message
        });
    }

    findOneByUsername(username)
        .then(checkPassword)
        .catch(onError);
};