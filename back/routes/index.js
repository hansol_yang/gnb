var router = require('express').Router();

var users = require('./api/users');
var posts = require('./api/posts');
var devNotes = require('./api/devNotes');
var notices = require('./api/notices');
var authController = require('./api/auth/auth.controller');
var auth = require('./api/auth');


router.use('/users', users);
router.use('/posts', posts);
router.use('/devNotes', devNotes);
router.use('/notices', notices);

router.use('/auth/getUserInfo', auth);
router.get('/auth/getUserInfo', authController.getUserInfo);


module.exports = router;