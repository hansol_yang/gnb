import React from 'react';
import {Link} from 'react-router-dom';
import CreateButton from 'material-ui/FloatingActionButton';
import IconCreate from 'material-ui/svg-icons/content/create';
import {connect} from 'react-redux';

import Header from '../components/board/Header';
import List from '../components/board/List';
import {readAllPostsRequest} from '../../redux/actions/posts/readAllPosts';


class Board extends React.Component {
    componentDidMount() {
        this.props.readAllPostsRequest();
    }

    render() {
        return (
            <div id="BoardContainer">
                <Header/>
                <List data={this.props.posts} before={this.props.match.url.substr(1)}/>
                {localStorage.token ?
                    <Link
                        to={
                            {
                                pathname: '/addNew',
                                state: {
                                    before: this.props.match.url
                                }
                            }
                        }
                        className="createButton"
                    >
                        <CreateButton
                            backgroundColor="black"
                        >
                            <IconCreate/>
                        </CreateButton>
                    </Link>
                    :
                    undefined
                }
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        posts: state.postsReducer.posts
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        readAllPostsRequest: () => dispatch(readAllPostsRequest())
    }
};

Board = connect(mapStateToProps, mapDispatchToProps)(Board);


export default Board;