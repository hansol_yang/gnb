import React from 'react';
import {Link} from 'react-router-dom';

import Signin from '../components/auth/Signin';
import Signup from '../components/auth/Signup';


class Auth extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            authType: false //false: Signin, true: Signup
        };

        this._handleAuthType = this._handleAuthType.bind(this);
    }

    _handleAuthType() {
        this.setState({
            authType: !this.state.authType
        })
    }

    render() {
        return (
            <div id="AuthContainer">
                <section className="background"></section>
                <Link id="logo" to="/">지앤비</Link>
                {this.state.authType ? <Signup handleType={this._handleAuthType}/> : <Signin handleType={this._handleAuthType}/>}
            </div>
        );
    }
}


export default Auth;