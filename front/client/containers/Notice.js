import React from 'react';
import Link from 'react-router-dom/Link';
import CreateButton from 'material-ui/FloatingActionButton';
import IconCreate from 'material-ui/svg-icons/content/create';
import {connect} from 'react-redux';

import Header from '../components/notice/Header';
import List from '../components/notice/List';
import {getUserInfoRequest} from '../../redux/actions/users/getUserInfo';
import {readAllNoticesRequest} from '../../redux/actions/notices/readAllNotices';


class Notice extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dummy: [
                {
                    title: '동아리방 청소를 매주 실시합니다.',
                    user: '김형석'
                },
                {
                    title: '회비 입금 계좌입니다.',
                    user: '김형석'
                }
            ],
            isAdmin: false
        };
    }

    componentDidMount() {
        this.props.getUserInfoRequest(localStorage.token);
        this.props.readAllNoticesRequest();
    }

    componentDidUpdate() {
        this.setState({
            isAdmin: this.props.result.result.isAdmin
        })
    }

    render() {
        return (
            <div id="NoticeContainer">
                <Header/>
                <List data={this.props.notices}/>
                {localStorage.token && this.state.isAdmin ?
                    <Link
                        to={
                            {
                                pathname: '/addNew',
                                state: {
                                    before: this.props.match.url
                                }
                            }
                        }
                        className="createButton"
                    >
                        <CreateButton
                            backgroundColor="black"
                        >
                            <IconCreate/>
                        </CreateButton>
                    </Link>
                    :
                    undefined
                }
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        result: state.usersReducer.result,
        notices: state.noticesReducer.notices
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        getUserInfoRequest: (token) => dispatch(getUserInfoRequest(token)),
        readAllNoticesRequest: () => dispatch(readAllNoticesRequest())
    }
};

Notice = connect(mapStateToProps, mapDispatchToProps)(Notice);


export default Notice;