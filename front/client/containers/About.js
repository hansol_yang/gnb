import React from 'react';

import Header from '../components/about/Header';
import Info from '../components/about/Info';


class About extends React.Component {
    render() {
        return (
            <div id="AboutContainer">
                <Header/>
                <Info/>
            </div>
        )
    }
}


export default About;