import React from 'react';

import Header from '../components/home/Header';
import Tab from "../components/home/Tab";


class Home extends React.Component {
    render() {
        return (
            <div id="HomeContainer">
                <Header/>
                <Tab/>
            </div>
        );
    }
}


export default Home;