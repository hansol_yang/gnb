import React from 'react';
import {connect} from 'react-redux';

import {readPostRequest} from '../../redux/actions/posts/readPost';
import {readDevNoteRequest} from '../../redux/actions/devNotes/readDevNote';


class Detail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            before: ''
        }
    }

    componentDidMount() {
        this.setState({
            before: this.props.location.state.before
        }, function () {
            switch (this.state.before) {
                case 'board':
                    this.props.readPostRequest(this.props.location.state.ref);
                    break;

                case 'dev_note':
                    this.props.readDevNoteRequest(this.props.location.state.ref);
                    break;

                default:
                    return 'PERMISSION DENIED';
            }
        })
    }

    render() {
        let _mapDataToComponent = () => {
            switch (this.state.before) {
                case 'board':
                    return {
                        type: '일반 게시판',
                        title: this.props.post.title,
                        body: this.props.post.body
                    };

                case 'dev_note':
                    return {
                        type: '개발 일지',
                        title: this.props.devNote.title,
                        body: this.props.devNote.body
                    };

                default:
                    return 'PERMISSION DENIED';
            }
        };

        return (
            <div id="DetailContainer">
                <p>{_mapDataToComponent().type}</p>
                <section className="detail">
                    <section className="element">
                        <p className="title">제목</p>
                        <p>{_mapDataToComponent().title}</p>
                    </section>
                    <section className="element">
                        <p className="body">내용</p>
                        <p>{_mapDataToComponent().body}</p>
                    </section>
                </section>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        post: state.postsReducer.post,
        devNote: state.devNotesReducer.devNote
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        readPostRequest: (id) => dispatch(readPostRequest(id)),
        readDevNoteRequest: (id) => dispatch(readDevNoteRequest(id))
    }
};

Detail = connect(mapStateToProps, mapDispatchToProps)(Detail);


export default Detail;