import React from 'react';
import {Link} from 'react-router-dom';
import CreateButton from 'material-ui/FloatingActionButton';
import IconCreate from 'material-ui/svg-icons/content/create';
import {connect} from 'react-redux';

import Header from '../components/devNote/Header';
import List from '../components/devNote/List';
import {readAllDevNotesRequest} from '../../redux/actions/devNotes/readAllDevNotes';


class DevNote extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dummy:[
                {
                    title: '지앤비 웹 제작기 1',
                    user: '양한솔'
                },
                {
                    title: '지앤비 웹 제작기 2',
                    user: '양한솔'
                },
                {
                    title: 'LWM2M 사용기 1',
                    user: '천보경'
                },
                {
                    title: 'Spring Web_Framework 사용기',
                    user: '주승환'
                }
            ]
        }
    }

    componentDidMount() {
        this.props.readAllDevNotesRequest();
    }

    render() {
        return (
            <div id="DevNoteContainer">
                <Header/>
                <List data={this.props.devNotes} before={this.props.match.url.substr(1)}/>
                {localStorage.token ?
                    <Link
                        to={
                            {
                                pathname: '/addNew',
                                state: {
                                    before: this.props.match.url
                                }
                            }
                        }
                        className="createButton"
                    >
                        <CreateButton
                            backgroundColor="black"
                        >
                            <IconCreate/>
                        </CreateButton>
                    </Link>
                    :
                    undefined
                }
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        devNotes: state.devNotesReducer.devNotes
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        readAllDevNotesRequest: () => dispatch(readAllDevNotesRequest())
    }
};

DevNote = connect(mapStateToProps, mapDispatchToProps)(DevNote);


export default DevNote;