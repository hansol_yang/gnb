import React from 'react';
import TextField from 'material-ui/TextField';
import SubmitButton from 'material-ui/RaisedButton';
import {connect} from 'react-redux';

import {createPostRequest} from '../../redux/actions/posts/createPost';
import {createDevNoteRequest} from '../../redux/actions/devNotes/createDevNote';
import {createNoticeRequest} from '../../redux/actions/notices/createNotice';


class AddNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            body: '',
            from: ''
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleSubmit() {
        switch (this.state.from) {
            case 'board':
                this.props.createPostRequest(this.state.title, this.state.body);
                history.back();
                break;

            case 'dev_note':
                this.props.createDevNoteRequest(this.state.title, this.state.body);
                history.back();
                break;

            case 'notice':
                this.props.createNoticeRequest(this.state.title, this.state.body);
                history.back();
                break;

            default:
                return alert('PERMISSION DENIED');
        }
    }

    componentDidMount() {
        this.setState({
            from: this.props.location.state.before.substr(1)
        })
    }

    render() {
        let submitButtonLabelStyle = {
            fontFamily: 'main',
            color: 'white'
        };

        let _mapTitleToComponent = () => {
            switch (this.state.from) {
                case 'board':
                    return '일반 게시판';

                case 'dev_note':
                    return '개발 일지';

                case 'notice':
                    return '공지 사항';

                default:
                    return;
            }
        };
        return (
            <div id="AddNewContainer">
                <section className="addNewForm">
                    <section className="title">
                        <p>{_mapTitleToComponent()}</p>
                    </section>
                    <section className="element">
                        <p className="title">
                            제목
                        </p>
                        <TextField
                            name="title"
                            hintText="제목"
                            onChange={this._handleChange}
                            value={this.state.title}
                            autoFocus
                            fullWidth
                        />
                    </section>
                    <section className="element">
                        <p className="title">
                            내용
                        </p>
                        <TextField
                            name="body"
                            hintText="내용"
                            rows={7}
                            onChange={this._handleChange}
                            value={this.state.body}
                            multiLine
                            fullWidth
                        />
                    </section>
                </section>
                <SubmitButton
                    backgroundColor="black"
                    label="완료"
                    labelStyle={submitButtonLabelStyle}
                    onClick={this._handleSubmit}
                    fullWidth
                />
            </div>
        );
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        createPostRequest: (title, body) => dispatch(createPostRequest(title, body)),
        createDevNoteRequest: (title, body) => dispatch(createDevNoteRequest(title, body)),
        createNoticeRequest: (title, body) => dispatch(createNoticeRequest(title, body))
    }
};

AddNew = connect(undefined, mapDispatchToProps)(AddNew);


export default AddNew;