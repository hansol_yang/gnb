import React from 'react';

import Header from '../components/schedule/Header';
import List from '../components/schedule/List';


class Schedule extends React.Component {
    render() {
        return (
            <div id="ScheduleContainer">
                <Header/>
                <List/>
            </div>
        )
    }
}


export default Schedule;