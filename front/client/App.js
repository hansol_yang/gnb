import React from 'react';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';

import Home from './containers/Home';
import About from './containers/About';
import Notice from './containers/Notice';
import DevNote from './containers/DevNote';
import Board from './containers/Board';
import Schedule from './containers/Schedule';
import Auth from './containers/Auth';
import AddNew from './containers/AddNew';
import Detail from './containers/Detail';
import NavBar from "./components/common/NavBar";
import Footer from './components/common/Footer'


const history = createHistory();

class App extends React.Component {
    render() {
        return (
            <Router history={history}>
                <div id="appRoot">
                    {/*{window.location.pathname === "/welcome" ? undefined : <NavBar/>}*/}
                    <NavBar/>
                    <Route exact path="/" render={() => <Home/>}/>
                    <Route path="/about" render={() => <About/>}/>
                    <Route path="/notice" component={Notice}/>
                    <Route path="/dev_note" component={DevNote}/>
                    <Route path="/board" component={Board}/>
                    <Route path="/schedule" render={() => <Schedule/>}/>
                    <Route path="/welcome" render={() =>
                        !localStorage.token ? <Auth/> : <Redirect to="/"/>
                    }/>
                    <Route path='/addNew' component={AddNew}/>
                    <Route path="/detail" component={Detail}/>
                    {/*{window.location.pathname === "/welcome" ? undefined : <Footer/>}*/}
                    <Footer/>
                </div>
            </Router>
        );
    }
}


export default App;