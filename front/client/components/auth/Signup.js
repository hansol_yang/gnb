import React from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/RaisedButton';
import {connect} from 'react-redux';
import Toast from 'material-ui/Snackbar';

import {signupRequest} from '../../../redux/actions/users/signup';


class Signup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password1: '',
            password2: '',
            msg: '',
            isOpen: false
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleToastClose = this._handleToastClose.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    _handleSubmit() {
        if (this.state.password1 === this.state.password2) {
            this.props.signupRequest(this.state.username, this.state.password1);
            this.setState({
                username: '',
                password1: '',
                password2: ''
            })
        } else {
            this.setState({
                password1: '',
                password2: ''
            }, () => alert('비밀번호를 다시 확인해주세요'));
        }
    }

    _handleToastClose() {
        this.setState({
            msg: '',
            isOpen: false
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            msg: nextProps.result.msg,
            isOpen: true
        }, function() {
            if(nextProps.result.code === 940924)
                this.props.handleType();
        })
    }


    render() {
        let signupFormPaperStyle = {
            position: 'absolute',
            left: '0',
            top: '30%',
            width: '100%',
            padding: '1rem',
            textAlign: 'center',
            opacity: '.9'
        };

        let _mapFieldToComponent = (name, label, type, hint) => (
            <TextField
                floatingLabelText={label}
                type={type}
                name={name}
                hintText={hint}
                style={
                    {
                        marginBottom: '1rem'
                    }
                }
                value={this.state[name]}
                onChange={this._handleChange}
                fullWidth
            />
        );

        return (
            <Paper
                zDepth={1}
                style={signupFormPaperStyle}
            >
                <p className="authType">회원가입</p>
                {_mapFieldToComponent('username', '이름', 'text', '홍길동')}
                {_mapFieldToComponent('password1', '비밀번호', 'password', '********')}
                {_mapFieldToComponent('password2', '비밀번호 확인', 'password', '********')}
                <Button
                    fullWidth
                    label="회원가입"
                    backgroundColor="black"
                    labelStyle={{fontFamily: 'main', color: "white"}}
                    style={{marginBottom: '1rem'}}
                    onClick={this._handleSubmit}
                />
                <p className="authQuestionFore">회원이신가요?&nbsp;</p>
                <p className="authQuestionBack" onClick={this.props.handleType}>로그인</p>
                <Toast
                    message={this.state.msg}
                    open={this.state.isOpen}
                    autoHideDuration={2500}
                    onRequestClose={this._handleToastClose}
                />
            </Paper>
        );
    }
}

Signup.propTypes = {
    handleType: React.PropTypes.func
};

Signup.defaultProps = {
    handleType: () => console.log('handleType is not defined yet')
};

let mapStateToProps = (state) => {
    return {
        result: state.usersReducer.result
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        signupRequest: (username, password) => dispatch(signupRequest(username, password))
    }
};

Signup = connect(mapStateToProps, mapDispatchToProps)(Signup);


export default Signup;