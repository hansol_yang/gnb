import React from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/RaisedButton';
import {connect} from 'react-redux';
import Toast from 'material-ui/Snackbar';

import {signinRequest} from '../../../redux/actions/users/signin';


class Signin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            msg: '',
            isOpen: false
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleToastClose = this._handleToastClose.bind(this);
    }

    _handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    _handleSubmit() {
        this.props.signinRequest(this.state.username, this.state.password);
    }

    _handleToastClose() {
        this.setState({
            msg: '',
            isOpen: false
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            msg: nextProps.result.msg,
            isOpen: true,
            username: '',
            password: ''
        }, function() {
            if(nextProps.result.code === 930709) {
                localStorage.token = nextProps.result.token;
                window.location.pathname = '/';
            }

        })
    }

    render() {
        let signinFormPaperStyle = {
            position: 'absolute',
            left: '0',
            top: '30%',
            width: '100%',
            padding: '1rem',
            textAlign: 'center',
            opacity: '.9'
        };

        let _mapFieldToComponent = (name, label, type, hint) => (
            <TextField
                floatingLabelText={label}
                type={type}
                name={name}
                hintText={hint}
                style={
                    {
                        marginBottom: '1rem'
                    }
                }
                value={this.state[name]}
                onChange={this._handleChange}
                fullWidth
            />
        );

        return (
            <Paper
                zDepth={1}
                style={signinFormPaperStyle}
            >
                <p className="authType">로그인</p>
                {_mapFieldToComponent("username", "이름", "text", "홍길동")}
                {_mapFieldToComponent("password", "비밀번호", "password", "********")}
                <Button
                    fullWidth
                    label="로그인"
                    backgroundColor="black"
                    labelStyle={{fontFamily: 'main', color: "white"}}
                    style={{marginBottom: '1rem'}}
                    onClick={this._handleSubmit}
                />
                <p className="authQuestionFore">처음이신가요?&nbsp;</p>
                <p className="authQuestionBack" onClick={this.props.handleType}>회원가입</p>
                <Toast
                    message={this.state.msg}
                    open={this.state.isOpen}
                    autoHideDuration={2500}
                    onRequestClose={this._handleToastClose}
                />
            </Paper>
        );
    }
}

Signin.propTypes = {
    handleType: React.PropTypes.func
};

Signin.defaultProps = {
    handleType: () => console.log('handleType is not defined yet')
};

let mapStateToProps = (state) => {
    return {
        result: state.usersReducer.result
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        signinRequest: (username, password) => dispatch(signinRequest(username, password))
    }
};

Signin = connect(mapStateToProps, mapDispatchToProps)(Signin);


export default Signin;