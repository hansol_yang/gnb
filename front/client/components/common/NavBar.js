import React from 'react';
import {Link} from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/IconButton';
import IconMenu from 'material-ui/svg-icons/navigation/menu';
import IconAuth from 'material-ui/svg-icons/communication/vpn-key';
import IconSignout from 'material-ui/svg-icons/action/exit-to-app';
import IconHome from 'material-ui/svg-icons/action/home';
import IconGnb from 'material-ui/svg-icons/social/people';
import IconNotice from 'material-ui/svg-icons/action/announcement';
import IconDev from 'material-ui/svg-icons/action/code';
import IconPost from 'material-ui/svg-icons/action/list';
import IconSchedule from 'material-ui/svg-icons/action/date-range';
import IconBack from 'material-ui/svg-icons/navigation/chevron-left';
import Drawer from 'material-ui/Drawer';
import Menu, {MenuItem} from 'material-ui/Menu';


let sideBarElement = {
    fontFamily: 'main'
};

let appBarStyle = {
    backgroundColor: 'white'
};

const SideBar = ({open, handleFunc}) => (
    <Drawer
        docked={false}
        open={open}
        onRequestChange={handleFunc}
        openSecondary={true}
    >
        <Menu>
            <Link className="sideBarMenuElement" to="/">
                <MenuItem
                    style={sideBarElement}
                    onTouchTap={handleFunc}
                    leftIcon={<IconHome/>}
                >
                    홈
                </MenuItem>
            </Link>
            <Link className="sideBarMenuElement" to="/about">
                <MenuItem
                    style={sideBarElement}
                    onTouchTap={handleFunc}
                    leftIcon={<IconGnb/>}
                >
                    지앤비에 대하여
                </MenuItem>
            </Link>
            <Link className="sideBarMenuElement" to="/notice">
                <MenuItem
                    style={sideBarElement}
                    onTouchTap={handleFunc}
                    leftIcon={<IconNotice/>}
                >
                    공지 사항
                </MenuItem>
            </Link>
            <Link className="sideBarMenuElement" to="/dev_note">
                <MenuItem
                    style={sideBarElement}
                    onTouchTap={handleFunc}
                    leftIcon={<IconDev/>}
                >
                    개발 일지
                </MenuItem>
            </Link>
            <Link className="sideBarMenuElement" to="/board">
                <MenuItem
                    style={sideBarElement}
                    onTouchTap={handleFunc}
                    leftIcon={<IconPost/>}
                >
                    일반 게시판
                </MenuItem>
            </Link>
            <Link className="sideBarMenuElement" to="/schedule">
                <MenuItem
                    style={sideBarElement}
                    onTouchTap={handleFunc}
                    leftIcon={<IconSchedule/>}
                >
                    일정
                </MenuItem>
            </Link>
            {localStorage.token ?
                <Link className="sideBarMenuElement" to="#">
                    <MenuItem
                        style={sideBarElement}
                        onTouchTap={handleFunc}
                        leftIcon={<IconSignout/>}
                        onClick={
                            () => {
                                if(confirm('정말 로그아웃하시겠습니까?')) {
                                    localStorage.clear();
                                    window.location.pathname = '/';
                                }
                            }
                        }
                    >
                        로그아웃
                    </MenuItem>
                </Link>
                :
                <Link className="sideBarMenuElement" to="/welcome">
                    <MenuItem
                        style={sideBarElement}
                        onTouchTap={handleFunc}
                        leftIcon={<IconAuth/>}
                    >
                        로그인
                    </MenuItem>
                </Link>
            }
        </Menu>
    </Drawer>
);

class NavBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };

        this._handleSidebarToggle = this._handleSidebarToggle.bind(this);
    }

    _handleSidebarToggle() {
        this.setState({
            open: !this.state.open
        })
    }

    render() {
        let IconButtonMenu = () => (
            <Button onClick={this._handleSidebarToggle}>
                <IconMenu/>
            </Button>
        );

        let IconButtonBack = () => (
            <Button onClick={() => history.back()}>
                <IconBack/>
            </Button>
        );
        return (
            <div>
                <AppBar
                    style={appBarStyle}
                    iconElementLeft={
                        history.state !== null && history.state.state ?
                            <IconButtonBack/>
                            :
                            undefined
                    }
                    iconElementRight={<IconButtonMenu/>}
                />
                <SideBar
                    open={this.state.open}
                    handleFunc={this._handleSidebarToggle}
                />
            </div>
        );
    }
}


export default NavBar;