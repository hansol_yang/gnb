import React from 'react';
import {Tabs, Tab as MTab} from 'material-ui/Tabs';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';


let tabElementStyle = {
    backgroundColor: 'white',
    color: 'black',
    fontFamily: 'main'
};

let listStyle = {
    fontFamily: 'main'
};

const Notice = ({data}) => (
    <section className="tabContent">
        <List>
            {data.map((notice, i) => (
                <div key={i}>
                    <ListItem
                        style={listStyle}
                        primaryText={notice.title}
                        secondaryText={notice.user}
                        leftAvatar={
                            <Avatar src="/images/home/header_background.jpg"/>
                        }
                    />
                    <Divider/>
                </div>
            ))}
        </List>
    </section>
);

const RecentPost = ({data}) => (
    <section className="tabContent">
        <List>
            {data.map((notice, i) => (
                <div key={i}>
                    <ListItem
                        style={listStyle}
                        primaryText={notice.title}
                        secondaryText={notice.user}
                        leftAvatar={
                            <Avatar src="/images/home/header_background.jpg"/>
                        }
                    />
                    <Divider/>
                </div>
            ))}
        </List>
    </section>
);

class Tab extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            noticeDummy: [
                {
                    title: '동아리방 청소를 매주 실시합니다.',
                    user: '김형석'
                },
                {
                    title: '회비 입금 계좌입니다.',
                    user: '김형석'
                }
            ],
            recentPostDummy: [
                {
                    title: '지앤비 웹 제작기 1',
                    user: '양한솔'
                },
                {
                    title: '지앤비 웹 제작기 2',
                    user: '양한솔'
                },
                {
                    title: 'LWM2M 사용기 1',
                    user: '천보경'
                },
                {
                    title: 'Spring Web_Framework 사용기',
                    user: '주승환'
                }
            ]
        }
    }

    render() {
        return (
            <Tabs>
                <MTab
                    style={tabElementStyle}
                    label="공지 사항"
                >
                    <Notice
                        data={this.state.noticeDummy}
                    />
                </MTab>
                <MTab
                    label="최근 게시물"
                    style={tabElementStyle}
                >
                    <RecentPost
                        data={this.state.recentPostDummy}
                    />
                </MTab>
            </Tabs>
        );
    }
}


export default Tab;