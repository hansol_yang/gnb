import React from 'react';


const Info = () => (
    <section className="info">
        <section className="infoElement">
            <p className="title">지앤비는</p>
            <p className="content">알 지(知) 와 날 비(飛) 의 합성어입니다</p>
        </section>
        <section className="infoElement">
            <p className="title">목적</p>
            <p className="content">모두가 <b>서버 프로그래밍</b>과 <b>웹 프로그래밍</b>에<br/>자신감이 있게 하고자 한다 </p>
        </section>
        <section className="infoElement">
            <p className="title">회장</p>
            <img src="/images/about/host.JPG"/>
            <p className="content">이름: 김형석</p>
            <p className="content">연락처: 010-4768-3144</p>
        </section>
        <section className="infoElement">
            <p className="title">릴리즈</p>
            <section className="released"></section>
        </section>
    </section>
);


export default Info;