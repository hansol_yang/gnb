import React from 'react';
import SubHeader from 'material-ui/Subheader';
import {List as MuiList, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';


let listStyle = {
    fontFamily: 'main'
};

class List extends React.Component {
    render() {
        return (
            <section className="list">
                <SubHeader>주요 일정</SubHeader>
                <section className="main">
                    <p>주요 일정으로 등록된 일정이 없습니다</p>
                </section>
                <SubHeader>전체 일정</SubHeader>
                <section className="all">
                    <MuiList>
                        <ListItem
                            style={listStyle}
                            primaryText={"SKT 아이디어톤"}
                            secondaryText={"170303 - 170408"}
                        />
                        <Divider/>
                        <ListItem
                            style={listStyle}
                            primaryText={"졸업 프로젝트 - 오벤져스"}
                            secondaryText={"170303 - 171230"}
                        />
                        <Divider/>
                    </MuiList>
                </section>
            </section>
        );
    }
}


export default List;