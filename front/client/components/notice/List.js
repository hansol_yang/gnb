import React from 'react';
import {List as MuiList, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';


let listStyle = {
    fontFamily: 'main'
};

const List = ({data, cb}) => (
    <section className="list">
        <MuiList>
            {data.map((notice, i) => (
                <div key={i}>
                    <ListItem
                        style={listStyle}
                        primaryText={notice.title}
                        secondaryText={notice._creator.username}
                        leftAvatar={
                            <Avatar src="/images/home/header_background.jpg"/>
                        }
                    />
                    <Divider/>
                </div>
            ))}
        </MuiList>
    </section>
);


export default List;