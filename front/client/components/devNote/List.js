import React from 'react';
import {List as MuiList, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import {Link} from 'react-router-dom';


let listStyle = {
    fontFamily: 'main'
};

const List = ({data, before}) => (
    <section className="list">
        <MuiList>
            {data.map((devNote, i) => (
                <Link
                    to={
                        {
                            pathname: "/detail",
                            state: {
                                ref: devNote._id,
                                before: before
                            }
                        }
                    }

                    style={{textDecoration: 'none'}}
                    key={i}
                >
                    <ListItem
                        style={listStyle}
                        primaryText={devNote.title}
                        secondaryText={devNote._creator.username}
                        leftAvatar={
                            <Avatar src="/images/home/header_background.jpg"/>
                        }
                    />
                    <Divider/>
                </Link>
            ))}
        </MuiList>
    </section>
);


export default List;