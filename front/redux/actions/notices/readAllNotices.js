import axios from 'axios';

import {READ_ALL_NOTICES, READ_ALL_NOTICES_SUCCESS, READ_ALL_NOTICES_FAILURE} from './ActionTypes';


function readAllNotices() {
    return {
        type: READ_ALL_NOTICES
    }
}

function readAllNoticesSuccess(data) {
    return {
        type: READ_ALL_NOTICES_SUCCESS,
        notices: data
    }
}

function readAllNoticesFailure(err) {
    return {
        type: READ_ALL_NOTICES_FAILURE,
        error: err
    }
}

export function readAllNoticesRequest() {
    return function(dispatch) {
        dispatch(readAllNotices());

        return axios({
            method: 'get',
            url: '/notices'
        }).then(function(response) {
            console.log(response.data);
            dispatch(readAllNoticesSuccess(response.data.result));
        }).catch(function(error) {
            dispatch(readAllNoticesFailure(error));
        })
    }
}