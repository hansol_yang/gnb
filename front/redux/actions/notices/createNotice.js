import axios from 'axios';

import {CREATE_NOTICE, CREATE_NOTICE_SUCCESS, CREATE_NOTICE_FAILURE} from './ActionTypes';


function createNotice() {
    return {
        type: CREATE_NOTICE
    }
}

function createNoticeSuccess(data) {
    return {
        type: CREATE_NOTICE_SUCCESS,
        result: data
    }
}

function createNoticeFailure(err) {
    return {
        type: CREATE_NOTICE_FAILURE,
        error: err
    }
}

export function createNoticeRequest(title, body) {
    return function(dispatch) {
        dispatch(createNotice());

        return axios({
            method: 'post',
            url: '/notices',
            data: {title, body},
            headers: {'x-access-token': localStorage.token}
        }).then(function(response) {
            console.log(response.data);
            dispatch(createNoticeSuccess(response.data));
        }).catch(function(error) {
            dispatch(createNoticeFailure(error));
        })
    }
}