import axios from 'axios';

import {CREATE_POST, CREATE_POST_SUCCESS, CREATE_POST_FAILURE} from './ActionTypes';


function createPost() {
    return {
        type: CREATE_POST
    }
}

function createPostSuccess() {
    return {
        type: CREATE_POST_SUCCESS
    }
}

function createPostFailure(err) {
    return {
        type: CREATE_POST_FAILURE,
        error: err
    }
}

export function createPostRequest(title, body) {
    return function(dispatch) {
        dispatch(createPost());

        return axios({
            method: 'post',
            data: {title, body},
            url: '/posts',
            headers: {'x-access-token': localStorage.token}
        }).then(function(response) {
            console.log(response.data);
            if(response.data.msg === 'jwt expired') {
                localStorage.clear();
            }
            dispatch(createPostSuccess());
        }).catch(function(error) {
            dispatch(createPostFailure(error))
        })
    }
}