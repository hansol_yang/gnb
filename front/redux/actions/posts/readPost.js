import axios from 'axios';

import {READ_POST, READ_POST_SUCCESS, READ_POST_FAILURE} from './ActionTypes';


function readPost() {
    return {
        type: READ_POST
    }
}

function readPostSuccess(data) {
    return {
        type: READ_POST_SUCCESS,
        post: data
    }
}

function readPostFailure(err) {
    return {
        type: READ_POST_FAILURE,
        error: err
    }
}

export function readPostRequest(id) {
    return function(dispatch) {
        dispatch(readPost());

        return axios({
            method: 'get',
            url: `/posts/${id}`
        }).then(function(response) {
            console.log(response.data);
            dispatch(readPostSuccess(response.data.post));
        }).catch(function(error) {
            dispatch(readPostFailure(error));
        })
    }
}

