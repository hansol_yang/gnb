/* CREATE */
export const CREATE_POST = 'CREATE_POST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';


/* READ ALL */
export const READ_ALL_POSTS = 'READ_ALL_POSTS';
export const READ_ALL_POSTS_SUCCESS = 'READ_ALL_POSTS_SUCCESS';
export const READ_ALL_POSTS_FAILURE = 'READ_ALL_POSTS_FAILURE';


/* READ */
export const READ_POST = 'READ_POST';
export const READ_POST_SUCCESS = 'READ_POST_SUCCESS';
export const READ_POST_FAILURE = 'READ_POST_FAILURE';