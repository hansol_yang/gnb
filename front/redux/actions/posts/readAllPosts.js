import axios from 'axios';

import {READ_ALL_POSTS, READ_ALL_POSTS_SUCCESS, READ_ALL_POSTS_FAILURE} from './ActionTypes';


function readAllPosts() {
    return {
        type: READ_ALL_POSTS
    }
}

function readAllPostsSuccess(data) {
    return {
        type: READ_ALL_POSTS_SUCCESS,
        posts: data
    }
}

function readAllPostsFailure(err) {
    return {
        type: READ_ALL_POSTS_FAILURE,
        error: err
    }
}

export function readAllPostsRequest() {
    return function(dispatch) {
        dispatch(readAllPosts());

        return axios({
            method: 'get',
            url: '/posts'
        }).then(function(response) {
            console.log(response.data);
            dispatch(readAllPostsSuccess(response.data.posts));
        }).catch(function(error) {
            dispatch(readAllPostsFailure(error));
        })
    }
}