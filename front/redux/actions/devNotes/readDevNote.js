import axios from 'axios';

import {READ_DEV_NOTE, READ_DEV_NOTE_SUCCESS, READ_DEV_NOTE_FAILURE} from './ActionsTypes';


function readDevNote() {
    return {
        type: READ_DEV_NOTE
    }
}

function readDevNoteSuccess(data) {
    return {
        type: READ_DEV_NOTE_SUCCESS,
        devNote: data
    }
}

function readDevNoteFailure(err) {
    return {
        type: READ_DEV_NOTE_FAILURE,
        error: err
    }
}

export function readDevNoteRequest(id) {
    return function(dispatch) {
        dispatch(readDevNote());

        return axios({
            method: 'get',
            url: `/devNotes/${id}`
        }).then(function(response) {
            console.log(response.data);
            dispatch(readDevNoteSuccess(response.data.devNote));
        }).catch(function(error) {
            dispatch(readDevNoteFailure(error));
        })
    }
}