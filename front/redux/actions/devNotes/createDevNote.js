import axios from 'axios';

import {CREATE_DEV_NOTE, CREATE_DEV_NOTE_SUCCESS, CREATE_DEV_NOTE_FAILURE} from './ActionsTypes';


function createDevNote() {
    return {
        type: CREATE_DEV_NOTE
    }
}

function createDevNoteSuccess() {
    return {
        type: CREATE_DEV_NOTE_SUCCESS
    }
}

function createDevNoteFailure(err) {
    return {
        type: CREATE_DEV_NOTE_FAILURE,
        error: err
    }
}

export function createDevNoteRequest(title, body) {
    return function(dispatch) {
        dispatch(createDevNote());

        return axios({
            method: 'post',
            url: '/devNotes',
            data: {title, body},
            headers: {'x-access-token': localStorage.token}
        }).then(function(response) {
            console.log(response.data);
            if(response.data.msg === 'jwt expired') {
                localStorage.clear();
            }
            dispatch(createDevNoteSuccess());
        }).catch(function(error) {
            dispatch(createDevNoteFailure(error));
        })
    }
}