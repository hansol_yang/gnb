import axios from 'axios';

import {READ_ALL_DEV_NOTES, READ_ALL_DEV_NOTES_SUCCESS, READ_ALL_DEV_NOTES_FAILURE} from './ActionsTypes';


function readAllDevNotes() {
    return {
        type: READ_ALL_DEV_NOTES
    }
}

function readAllDevNotesSuccess(data) {
    return {
        type: READ_ALL_DEV_NOTES_SUCCESS,
        devNotes: data
    }
}

function readAllDevNotesFailure(err) {
    return {
        type: READ_ALL_DEV_NOTES_FAILURE,
        error: err
    }
}

export function readAllDevNotesRequest() {
    return function(dispatch) {
        dispatch(readAllDevNotes());

        return axios({
            method: 'get',
            url: '/devNotes'
        }).then(function(response) {
            console.log(response.data);
            dispatch(readAllDevNotesSuccess(response.data.devNotes));
        }).catch(function(error) {
            dispatch(readAllDevNotesFailure(error));
        })
    }
}