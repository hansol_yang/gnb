/* CREATE */
export const CREATE_DEV_NOTE = 'CREATE_DEV_NOTE';
export const CREATE_DEV_NOTE_SUCCESS = 'CREATE_DEV_NOTE_SUCCESS';
export const CREATE_DEV_NOTE_FAILURE = 'CREATE_DEV_NOTE_FAILURE';


/* READ ALL */
export const READ_ALL_DEV_NOTES = 'READ_ALL_DEV_NOTES';
export const READ_ALL_DEV_NOTES_SUCCESS = 'READ_ALL_DEV_NOTES_SUCCESS';
export const READ_ALL_DEV_NOTES_FAILURE = 'READ_ALL_DEV_NOTES_FAILURE';


/* READ */
export const READ_DEV_NOTE = 'READ_DEV_NOTE';
export const READ_DEV_NOTE_SUCCESS = 'READ_DEV_NOTE_SUCCESS';
export const READ_DEV_NOTE_FAILURE = 'READ_DEV_NOTE_FAILURE';