import axios from 'axios';

import {SIGNUP_FAILURE, SIGNUP_SUCCESS, SIGNUP} from './ActionTypes';


function signup() {
    return {
        type: SIGNUP
    }
}

function signupSuccess(data) {
    return {
        type: SIGNUP_SUCCESS,
        result: data
    }
}

function signupFailure(err) {
    return {
        type: SIGNUP_FAILURE,
        error: err
    }
}

export function signupRequest(username, password) {
    return function(dispatch) {
        dispatch(signup());

        return axios({
            method: 'post',
            url: '/users/register',
            data: {username, password}
        }).then(function(response) {
            console.log(response.data);
            dispatch(signupSuccess(response.data));
        }).catch(function(error) {
            dispatch(signupFailure());
        })
    }
}