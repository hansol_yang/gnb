import axios from 'axios';

import {GET_USER_INFO, GET_USER_INFO_SUCCESS, GET_USER_INFO_FAILURE} from './ActionTypes';


function getUserInfo() {
    return {
        type: GET_USER_INFO
    }
}

function getUserInfoSuccess(data) {
    return {
        type: GET_USER_INFO_SUCCESS,
        result: data
    }
}

function getUserInfoFailure(err) {
    return {
        type: GET_USER_INFO_FAILURE,
        error: err
    }
}

export function getUserInfoRequest(token) {
    return function (dispatch) {
        dispatch(getUserInfo());

        return axios({
            method: 'get',
            url: '/auth/getUserInfo',
            headers: {'x-access-token': token}
        }).then(function(response) {
            console.log(response.data);
            dispatch(getUserInfoSuccess(response.data));
        }).catch(function(error) {
            dispatch(getUserInfoFailure(error));
        })
    }
}