import axios from 'axios';

import {SIGNIN, SIGNIN_SUCCESS, SIGNIN_FAILURE} from './ActionTypes';


function signin() {
    return {
        type: SIGNIN
    }
}

function signinSuccess(data) {
    return {
        type: SIGNIN_SUCCESS,
        result: data
    }
}

function signinFailure(err) {
    return {
        type: SIGNIN_FAILURE,
        error: err
    }
}

export function signinRequest(username, password) {
    return function (dispatch) {
        dispatch(signin());

        return axios({
            method: 'post',
            url: '/users/login',
            data: {username, password}
        }).then(function (response) {
            console.log(response.data);
            dispatch(signinSuccess(response.data));
        }).catch(function (error) {
            dispatch(signinFailure(error));
        })
    }
}