import update from 'react-addons-update';

import * as types from '../actions/posts/ActionTypes';


const initialState = {
    status: 'INIT',
    posts: [],
    post: {}
};

const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CREATE_POST:
        case types.READ_ALL_POSTS:
        case types.READ_POST:
            return update(state, {
                status: {$set: 'WAIT'}
            });

        case types.CREATE_POST_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'}
            });

        case types.READ_ALL_POSTS_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                posts: {$set: action.posts}
            });

        case types.READ_POST_SUCCESS:
            return update(state, {
                stats: {$set: 'SUCCESS'},
                post: {$set: action.post}
            });

        case types.CREATE_POST_FAILURE:
        case types.READ_ALL_POSTS_FAILURE:
        case types.READ_POST_FAILURE:
            return update(state, {
                status: {$set: 'FAILURE'}
            });

        default:
            return state;
    }
};


export default postsReducer;