import {combineReducers} from 'redux';

import usersReducer from './users';
import postsReducer from './posts';
import devNotesReducer from './devNotes';
import noticesReducer from './notices';


const rootReducer = combineReducers({
    usersReducer,
    postsReducer,
    devNotesReducer,
    noticesReducer
});


export default rootReducer;