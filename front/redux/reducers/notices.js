import update from 'react-addons-update';

import * as types from '../actions/notices/ActionTypes';


const initialState = {
    status: 'INIT',
    notices: []
};

const noticesReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CREATE_NOTICE:
        case types.READ_ALL_NOTICES:
            return update(state, {
                status: {$set: 'WAIT'}
            });

        case types.CREATE_NOTICE_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'}
            });

        case types.READ_ALL_NOTICES_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                notices: {$set: action.notices}
            });

        case types.CREATE_NOTICE_FAILURE:
        case types.READ_ALL_NOTICES_FAILURE:
            return update(state, {
                status: {$set: 'FAILURE'}
            });

        default:
            return state;
    }
};


export default noticesReducer;