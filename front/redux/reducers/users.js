import update from 'react-addons-update';

import * as types from '../actions/users/ActionTypes';


const initialState = {
    status: 'INIT',
    result: {}
};


const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SIGNUP:
        case types.SIGNIN:
        case types.GET_USER_INFO:
            return update(state, {
                status: {$set: 'WAIT'}
            });

        case types.SIGNUP_SUCCESS:
        case types.SIGNIN_SUCCESS:
        case types.GET_USER_INFO_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                result: {$set: action.result}
            });

        case types.SIGNUP_FAILURE:
        case types.SIGNIN_FAILURE:
        case types.GET_USER_INFO_FAILURE:
            return update(state, {
                status: {$set: 'FAILURE'}
            });

        default:
            return state;
    }
};


export default usersReducer;
