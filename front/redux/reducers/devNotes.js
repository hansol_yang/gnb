import update from 'react-addons-update';

import * as types from '../actions/devNotes/ActionsTypes';


const initialState = {
    status: 'INIT',
    devNotes: [],
    devNote: {}
};

const devNotesReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CREATE_DEV_NOTE:
        case types.READ_ALL_DEV_NOTES:
        case types.READ_DEV_NOTE:
            return update(state, {
                status: {$set: 'WAIT'}
            });

        case types.CREATE_DEV_NOTE_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'}
            });

        case types.READ_ALL_DEV_NOTES_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                devNotes: {$set: action.devNotes}
            });

        case types.READ_DEV_NOTE_SUCCESS:
            return update(state, {
                status: {$set: 'SUCCESS'},
                devNote: {$set: action.devNote}
            });

        case types.CREATE_DEV_NOTE_FAILURE:
        case types.READ_ALL_DEV_NOTES_FAILURE:
        case types.READ_DEV_NOTE_FAILURE:
            return update(state, {
                status: {$set: 'FAILURE'}
            });

        default:
            return state;
    }
};


export default devNotesReducer;