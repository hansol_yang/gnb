import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import App from './client/App';
import reducer from './redux/reducers/index';

import '../back/public/stylesheets/style.css';
import '../back/public/stylesheets/home/index.css';
import '../back/public/stylesheets/auth/index.css';
import '../back/public/stylesheets/dev_note/index.css';
import '../back/public/stylesheets/board/index.css';
import '../back/public/stylesheets/schedule/index.css';
import '../back/public/stylesheets/add_new/index.css';
import '../back/public/stylesheets/detail/index.css';
import '../back/public/stylesheets/home/header.css';
import '../back/public/stylesheets/about/header.css';
import '../back/public/stylesheets/notice/header.css';
import '../back/public/stylesheets/dev_note/header.css';
import '../back/public/stylesheets/board/header.css';
import '../back/public/stylesheets/schedule/header.css';
import '../back/public/stylesheets/home/tab.css';
import '../back/public/stylesheets/auth/sign_in.css';
import '../back/public/stylesheets/about/info.css';
import '../back/public/stylesheets/notice/list.css';
import '../back/public/stylesheets/dev_note/list.css';
import '../back/public/stylesheets/board/list.css';
import '../back/public/stylesheets/schedule/list.css';


injectTapEventPlugin();

const rootElement = document.getElementById('root');

const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
    <MuiThemeProvider>
        <Provider store={store}>
            <App/>
        </Provider>
    </MuiThemeProvider>,
    rootElement
);


if (module.hot) {
    module.hot.accept();
}